// Ajax helper.
// Adapted from http://www.sitepoint.com/guide-vanilla-ajax-without-jquery/
var ajaxRequest = function (url, callback, options) {
  var options = options || {};
  var type = options.type || 'GET';
  var send = options.send || null;

  var xhr = new XMLHttpRequest();
  xhr.open(type, url);
  xhr.send(send);

  xhr.onreadystatechange = function () {
    var DONE = 4;
    var OK = 200;
    if (xhr.readyState === DONE) {
      if (xhr.status === OK) {
        var data = JSON.parse(xhr.responseText);
        if (typeof callback === 'function') {
          callback({
            type: 'success',
            data: data
          });
        }
      } else {
        if (typeof callback === 'function') {
          callback({
            type: 'error',
            data: xhr.status
          });
        }
      }
    }
  }
};

// Helper for formatting currency.
// http://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
var numberWithCommas = function (x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


/**
 * Create widget constructor, which returns an instance of the widget, with an
 * api that can be accessed externally.
 */
var CrowdFundingWidget = function (options) {
  var options = options || {};

  // Create our top level api object.
  //
  // This object is returned as the api for our instance.
  var api = {
    // Do out initial widget setup.
    _init: function () {
      // Get parent element by ID.
      // On instantiating the widget an alternative widgetId can be passed in.
      var widgetId = options.widgetId || 'crowdFundingWidget';
      this.el = document.getElementById(widgetId);
      
      // Create empty data object.
      this.data = {};

      // Set default form state.
      this.formState = 'active';

      // Fetch data.
      this.fetchData(true);

      // Get dynamic els.
      this._getDynamicEls();

      // Return api.
      return this;
    },

    // Get all of our dynamic els.
    // 
    // They are all descendants of the top level widget element, and must all
    // have a `data-crowd-funding-widget-dynamic` data attribute. The value of
    // the `data-crowd-funding-widget-dynamic` attribute is the dynamic el's
    // key.
    // 
    // We then store all of the elements in a property of the module, keyed by
    // their dynamic el key.
    _getDynamicEls: function () {
      
      // Get dynamic els.
      var dynamicElsTemp = this.el.querySelectorAll('[data-crowd-funding-widget-dynamic]');

      // Create the empty object.
      this.dynamicEls = {};

      // We have to run through the dynamic els and check that we have a
      // corresponding controller for each. We then save them, keyed by the
      // dynamic el key.
      for (var i in dynamicElsTemp) {
        if (dynamicElsTemp.hasOwnProperty(i)) {
          var el = dynamicElsTemp[i];
          var dataKey = el.getAttribute('data-crowd-funding-widget-dynamic');
          
          if (dataKey && this._dynamicElsControllers.hasOwnProperty(dataKey)) {
            
            // Save to module property.
            this.dynamicEls[dataKey] = el;
          }
        }
      }
    },

    // Fetch data from the server and update the module's data store.
    // 
    // When data returns it will call ._onDataReady()
    // 
    // Currently no provision for the data request failing, we could show an
    // error message, or set a timeout to retry the fetch. 
    fetchData: function (first) {
      var first = first || false;
      // Request data.
      ajaxRequest('/api/crowdFundingPage', function (payload) {
        // If request is successful call ._onDataReady()
        if (payload.type === 'success') {
          api._onDataReady(payload.data);

          // If this is the first fetch, then call ._widgetReady().
          if (first) api._widgetReady();
        }
      });
    },

    // Called when data returns.
    _onDataReady: function (data) {
      // Set data store on module.
      this.data = data;

      // Update all dynamic elements.
      this._updateAllDynamicEls();
    },

    // Actions to happen on widget being ready.
    _widgetReady: function () {

      // Add ready class to widget for styling purposes.
      this.el.className += ' ready';
    },

    // Cycle through our dynamic elements and call ._updateAllDynamicEl() 
    // method, passing it the key of the dynamic element.
    _updateAllDynamicEls: function () {
      for (var key in this.dynamicEls) {
        this._updateAllDynamicEl(key);
      }
    },

    // Update a single dynamic element based on its key.
    _updateAllDynamicEl: function (key, args) {
      // Get args. if any.
      var args = args || null;

      // Check the dynamic el exists.
      if (!this.dynamicEls.hasOwnProperty(key)) return;

      // Call the dynamic el's controller, passing args.
      this._dynamicElsControllers[key](args);
    },

    // Update only the dynamic els affected by an update to the pledged amount.
    _onPledgedAmountUpdate: function () {
      this._updateAllDynamicEl('errorFeedback', null);
      var controllersToUpdate = ['pledged', 'percentFunded', 'progressBar'];
      for (var i in controllersToUpdate) {
        this._updateAllDynamicEl(controllersToUpdate[i]);
      }
    },

    // Handle the submission of the pledge form.
    _submitPledgeForm: function () {

      // Fetch value of the pledge input.
      var pledgeInput = api.dynamicEls['pledgeInput'];
      var pledgeAmount = parseInt(pledgeInput.value);
      
      // If we dont' have a valid number here then call the error handler and
      // return here.
      if (typeof pledgeAmount !== 'number' || isNaN(pledgeAmount)) {
        this._onSubmitPledgeFormError({
          error: 'invalid input',
          input: pledgeInput
        });
        return;
      }

      // If we've got this far then the input is valid, so reset any error
      // messages.
      this._updateAllDynamicEl('errorFeedback', null);

      // Update the state of the pledge form to `sending`. This greys out the
      // input, and blocks any form events.
      this._updatePledgeFormState('sending');

      // Call our AJAX helper, sending the amount to the api route.
      ajaxRequest('/api/pledge/' + pledgeAmount, function (payload) {

        if (payload.type === 'success') {
          // If return is successful, update the totalPledged property in our
          // data store, call `._onPledgedAmountUpdate()` to update the
          // appropriate dynamic els and set the pledge form state to
          // `submitted` - this will show the thank you message and disable &
          // hide the form input.
          var newTotalPledged = payload.data.totalPledged;
          api.data.totalPledged = newTotalPledged;
          api._onPledgedAmountUpdate();
          api._updatePledgeFormState('submitted');
        }
        else {
          // If we have an error, call our error handler, and set the pledge 
          // form state back to active.
          api._onPledgeError();
          api._updatePledgeFormState('active');
        }
      }, {
        type: 'POST'
      });
    },

    // Calls an update on the form wrapper element.
    _updatePledgeFormState: function (state) {
      this.formState = state;
      this._updateAllDynamicEl('formWrapper');
    },

    // Handle an unacceptable input value.
    _onSubmitPledgeFormError: function (data) {
      var message = "Please enter a numerical value."
      this._updateAllDynamicEl('errorFeedback', message);
    },

    // Handle a failed AJAX pledge request.
    _onPledgeError: function () {
      var message = "We're very sorry but your pledge was not received, please try again, or if you continue to have an issue, please contact our support team."
      this._updateAllDynamicEl('errorFeedback', message);
    },

    // The controller methods for our dynamic elements.
    // When called they fetch the element from the dynamicEls store, and update
    // it based on the current data store.
    // 
    // In the case of the form elements we also bind any actions.
    _dynamicElsControllers: {

      headerImg: function () {
        var el = api.dynamicEls['headerImg'];

        // Get and set image src.
        var headerImgPath = api.data.headerImgPath;
        el.src = headerImgPath;

        // Get and set image alt text.
        var altText = api.data.name;
        el.alt = altText;
      },

      storyText: function () {
        var el = api.dynamicEls['storyText'];
        var elText = api.data.story;
        el.innerHTML = elText;
      },

      pledged: function () {
        // Format the pledged amount as currency.
        var el = api.dynamicEls['pledged'];
        var elText = api.data.totalPledged;
        elText = '£' + numberWithCommas(elText) + '.00';
        el.innerHTML = elText;
      },

      pledgedTarget: function () {
        // Format the pledged target as currency.
        var el = api.dynamicEls['pledgedTarget'];
        var elText = api.data.target;
        elText = '£' + numberWithCommas(elText) + '.00';
        el.innerHTML = elText;
      },
      
      percentFunded: function () {
        // Calculate the percentage funded based on the totalPledged and the
        // target.
        var el = api.dynamicEls['percentFunded'];
        var percentage = (api.data.totalPledged / api.data.target) * 100;
        percentage = Math.round(percentage);
        percentage = percentage + '%';
        el.innerHTML = percentage;
      },

      progressBar: function () {
        // Calculate the width of the progress bar based on the progress
        // percentage.
        var el = api.dynamicEls['progressBar'];
        var percentage = (api.data.totalPledged / api.data.target) * 100;
        percentage = Math.round(percentage);
        if (percentage > 100) percentage = 100; // Limit to 100%.
        percentage = percentage + '%';

        // Delay so that animation is visible.
        window.setTimeout(function () {
          el.style.width = percentage;
        }, 100);
      },

      formWrapper: function () {
        // Update the class on the formWrapper to reflect the form's state.
        var el = api.dynamicEls['formWrapper'];
        var classes = el.className;
        classes = classes
          .replace('active', '')
          .replace('sending', '')
          .replace('submitted', '');

        // Append current formState to classes.
        classes += ' ' + api.formState;

        // Set update classes string to element.
        el.className = classes;
      },

      pledgeInput: function () {
        // Call `._submitPledgeForm()` on enter keyup.
        var el = api.dynamicEls['pledgeInput'];
        el.onkeyup = function (data) {
          // Only allow enter keyup to submit form if formState is active. This
          // prevents a second submission whilst the AJAX request is in-flight.
          if (api.formState === 'active') {
            if (data.keyCode === 13) {
              api._submitPledgeForm();
            }
          }
        };
      },

      submitButton: function () {
        // Call `._submitPledgeForm()` on clicking submit button.
        var el = api.dynamicEls['submitButton'];
        el.onclick = function () {
          // Only allow click to submit form if formState is active. This
          // prevents a second submission whilst the AJAX request is in-flight.
          if (api.formState === 'active') {
            api._submitPledgeForm();
          }
        };
      },

      errorFeedback: function (message) {
        // Set the errorFeedback el to the message passed.
        var message = message || null;
        var el = api.dynamicEls['errorFeedback'];
        el.innerHTML = message;
      },

      avatarImg: function () {
        // Set the avatar src and alt text.
        var el = api.dynamicEls['avatarImg'];

        // Get and set image src.
        var avatarImgPath = api.data.ownerAvatar;
        el.src = avatarImgPath;

        // Get and set image alt text.
        var altText = api.data.owner;
        el.alt = altText;
      }

    },
  };

  // Initialise widget and return.
  return api._init();
};

// Create an instance of the widget.
var crowdFundingWidget = CrowdFundingWidget();

