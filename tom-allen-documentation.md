# Tom Allen - FED technical test

## Approach
I have not used any libraries, plugins or preprocessors for this challenge. I have adapted a helper function for AJAX requests, and I have borrowed a bit of regex for formatting currency. Both are at the top of the `main.js` file and are credited.

My work is all in `/views/index.html`, `/assets/css/styles.css` and `/assets/javascript/main.js`. I have added a couple of properties to the mockdata, output by the API, as it was data that belonged with the specific project.

I have created an HTML view, which acts as the skeleton of the widget and contains any non-dynamic content, such as the widget's footer and any static labels. All content that is specific to the fund-raising project is pulled from the API request and then used to populate the view.

In order to target dynamic elements I have given them a data-attribute and a key, for example `data-crowd-funding-widget-dynamic="storyText"`. Each dynamic element has a corresponding controller function in the JavaScript widget.

On initialisation the JS widget constructor returns a new instance of the widget, the instance then fetches the initial data from the API route and calls the controllers for all dynamic elements, which update the corresponding element. The function of the controllers vary for each element. All are commented in the JS.

Each widget instance exposes its methods, most are private (denoted by a `_` prefix), however `.fetchData()` can be called to re-fetch data from the server. If I had more time it would be nice to implement a web sockets connection with the server so that this could be called when data changes on the server.

On submitting the pledge form, an AJAX POST request is sent to the server, and the returned `totalPledged` value is used to update the widget's data store and in turn update the dynamic elements which are affected by the `totalPledged` value. On POST errors the user is shown an error message, and allowed to re-submit the form. An error message is also show on the user trying to submit the form with an invalid input.

## If I had more time
As mentioned, I'd love to implement a web-sockets solution to keep the widget up to date with data changes on the server. I would also like to spend a little longer getting the layout just so. In production I would probably use a preprocessor for the CSS, this would allow me to use variables for colours, gutters and standard font sizes, and could handle browser prefixes. I would also add AMD and CommonJS exports to the widget JS file so that it could be integrated with a larger project using require JS or build scripts.

I would also have liked to do more in the way of cross-browser testing, I have done my best to use JS and CSS APIs that go back as far as possible.

Please let me know if you have any questions.

Tom Allen
tomjamesallenweb@gmail.com